let express = require("express"),
  app = express(),
  port = process.env.PORT || 3001;

let jwt = require("jsonwebtoken");

let path = require("path");

let requestJson = require("request-json");

let urlBaseMLab = "https://api.mlab.com/api/1/databases/eguzmant/collections";

let apiKeyMLab = "apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

let clientMlabBase;

let urlClients =
  "https://api.mlab.com/api/1/databases/eguzmant/collections/Clients?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var bodyParser = require("body-parser");

app.use(bodyParser.json());

app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Origin, X-Requested-With, Content-Type, Accept"
  );
  next();
});

var bcrypt = require("bcrypt");

app.listen(port);

console.log("todo list RESTful API server started on: " + port);

//QUERIES REGION

let setQuery = function (querykey, req, collectionName, res, next) {
  let collectionPath = "";
  let query = "";

  switch (querykey) {
    case "LGN": ///Login
      query = 'q={"number":"' + req.body.number + '"}';
      break;

    case "CDH": ///Valid user query
      query = 'q={"number":"' + req.params.cardholderid + '"}';
      break;

    case "WDW": ///Withdrawals by cardholder
    case "CTG": ///Categories by cardholder
      query = 'q={"cardholder.id":"' + req.params.cardholderid + '"}';
      break;

    /*case "CTG":
      query = 'q={"cardholder.id":"' + req.params.cardholderid + '"}';
      break;*/
  }

  collectionPath = "/" + collectionName + "?";

  let requestPath =
    urlBaseMLab +
    collectionPath +
    apiKeyMLab +
    (query != "" ? "&" + query : "");

  //res.send(requestPath);

  clientMlabBase = requestJson.createClient(requestPath);
};

//RESPONSE //////////////////////////////

let setResponse = function (err, res, body, msg) {
  if (!err) {
    if (body.length > 0) {
      res.status(200).send(body);
    } else {
      res.status(404).send(msg);
    }
  }
};

//ENCRYPT //////////////////////////////

let result = "";

let encrypt = function (val, next) {
  let BCRYPT_SALT_ROUNDS = 12;

  bcrypt
    .hash(val, BCRYPT_SALT_ROUNDS)
    .then(function (hashedPass) {
      console.log(hashedPass);
    })
    .catch(function (error) {
      console.log("Error encrypting value: ");
      console.log(error);
      next();
    });
};

app.post("/v0/bcrypt/encrypt", function (req, res, next) {
  var password = req.body.password;

  encrypt(password, next);
});

//CARDHOLDERS //////////////////////////////

///User validation (login)
app.post("/v0/login", function (req, res, next) {
  res.set("Access-Control-Allow-Headers", "Content-Type");

  setQuery("LGN", req, "Cardholder", res, next);

  clientMlabBase.get("", function (err, resM, body) {
    let pass = body.length > 0 ? body[0].password : "";
    bcrypt.compare(req.body.password, pass).then(function (match) {
      if (match) {
        const user = { number: req.body.number };

        jwt.sign({ user: user }, req.body.password, (err, token) => {
          res.set("usrwt", token);

          res.status(200).send(true);
        });
      } else {
        res.status(400).send(false);
      }
    });
  });
});

///Valid user query
app.get("/v0/cardholders/:cardholderid", function (req, res) {
  setQuery("CDH", req, "Cardholder", res);

  clientMlabBase.get("", function (err, resM, body) {
    setResponse(err, res, body, "Elemento no encontrado.");
  });
});

//WITHDRAWALS //////////////////////////////

///Withdrawals collection query
app.get("/v0/cardholders/:cardholderid/withdrawals", function (req, res) {
  setQuery("WDW", req, "Withdrawal", res);

  clientMlabBase.get("", function (err, resM, body) {
    setResponse(err, res, body, "No se encontró ningún movimiento.");
  });
});

///Withdrawal item query
app.get("/v0/cardholders/:cardholderid/withdrawals/:withdrawalid", function (
  req,
  res
) {
  setQuery("WDL", req, "Withdrawal", res);

  clientMlabBase.get("", function (err, resM, body) {
    setResponse(err, res, body, "No se encontró ningún movimiento.");
  });
});

///Withdrawal category update
app.patch("/v0/cardholders/:cardholderid/withdrawals", function (req, res) {
  setQuery("UWD", req, "Withdrawal", res);

  clientMlabBase.get("", function (err, resM, body) {
    setResponse(err, res, body, "No se encontró ningún movimiento.");
  });
});

//CATEGORIES //////////////////////////////

let setFormat = function (name, cardholderid) {
  let item = {
    id: "2",
    name: name,
    cardholder: {
      id: cardholderid,
    },
  };

  return JSON.stringify(item);
};

///Categories collection query
app.get("/v0/cardholders/:cardholderid/categories", function (req, res) {
  setQuery("CTG", req, "Category", res);

  clientMlabBase.get("", function (err, resM, body) {
    setResponse(err, res, body, "No se encontraron categorías.");
  });
});

///Category item query
app.get("/v0/cardholders/:cardholderid/categories/:categoryid", function (
  req,
  res
) {
  setQuery("CGY", req, "Category", res);

  clientMlabBase.get("", function (err, resM, body) {
    setResponse(err, res, body, "No se encontraron categorías.");
  });
});

///Category insertion
app.post("/v0/cardholders/:cardholderid/categories", function (req, res) {
  setQuery("SCG", req, "Category", res);
  req.body = setFormat();
  clientMlabBase.get("", function (err, resM, body) {
    setResponse(err, res, body, "No se encontraron categorías.");
  });
});

///Categories Update
app.put("/v0/cardholders/:cardholderid/categories", function (req, res) {
  setQuery("UCG", req, "Category", res);

  clientMlabBase.get("", function (err, resM, body) {
    setResponse(err, res, body, "No se encontraron categorías.");
  });
});
